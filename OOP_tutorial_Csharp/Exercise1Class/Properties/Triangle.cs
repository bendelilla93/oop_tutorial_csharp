﻿using System;
using System.Diagnostics;

namespace Exercise1Class
{
    public class Triangle
    {
        private string f_name;
        private double f_sideA;
        private double f_sideB;
        private double f_sideC;

        public string Name
        {
            get
            {
                return f_name;
            }
        }
        public double Area
        {
            get
            {
                return CalculateArea();
            }
        }
        public double Perimeter
        {
            get
            {
                return CalculatePerimeter();
            }
        }

        public Triangle(string name, double sideA, double sideB, double sideC)
        {
            f_name = name;
            f_sideA = sideA;
            f_sideB = sideB;
            f_sideC = sideC;
        }

        public double GetSide(char c)
        {
            switch (c) 
            {
                case 'a':
                case 'A':
                    return f_sideA;
                case 'b':
                case 'B':
                    return f_sideB;
                case 'c':
                case 'C':
                    return f_sideC;
                default:
                    Debug.Assert(false, "Invalid parameter...");
                    return 0;
            }
        }

        public override string ToString()
        {
            String x = String.Format("The name of the triangle : {0} \nSide A: {1}\nSide B: {2}\nSide C: {3}\nArea: {4}\nPerimeter: {5}",
                f_name, f_sideA, f_sideB, f_sideC, CalculateArea(), CalculatePerimeter());
            return x;
        }

        private double CalculateArea()
        {
            double sides = f_sideA * f_sideA + f_sideB * f_sideB - f_sideC * f_sideC;
            double twoab2 = 4 * f_sideA * f_sideA * f_sideB * f_sideB;
            double sin2 = 1 - sides * sides / twoab2;

            double area = f_sideA * f_sideB * Math.Sqrt(sin2) * 0.5;
            return area;
        }

        private double CalculatePerimeter()
        {
            return f_sideA + f_sideB + f_sideC;
        }
    }
}
