﻿using Exercise1Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_tutorial_Csharp
{
    class Program
    {
        static void Main(string[] args)
        {
            Triangle T1 = new Triangle("tri1", 5, 6, 7);
            Triangle T2 = new Triangle("tri2", 7, 8, 9);
            Triangle T3 = new Triangle("tri3", 9, 10, 11);
            Console.WriteLine(T1.ToString());
            Console.WriteLine(T2.ToString());
            Console.WriteLine(T3.ToString());
            Console.ReadLine();

            Triangle myTestTriangle = new Triangle("Pythagoras", 3, 4, 5);
            Console.WriteLine("--------------------------------");
            Console.WriteLine(myTestTriangle.Name);
            Console.WriteLine(myTestTriangle.Area);
            Console.WriteLine(myTestTriangle.Perimeter);
            Console.WriteLine(myTestTriangle.GetSide('a'));
            Console.WriteLine(myTestTriangle.GetSide('B'));
            Console.WriteLine(myTestTriangle.GetSide('c'));
            Console.WriteLine(myTestTriangle.GetSide('d'));
            Console.ReadLine();
        }
    }
}
